﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using CATI.Data;

namespace CATI.Wcf
{
   // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICatiService" in both code and config file together.
   [ServiceContract]
   public interface ICatiService
   {

      [OperationContract]
      IEnumerable<News> GetNews();
      
      [OperationContract]
      News GetNewsById(int newsId);

      [OperationContract]
      IEnumerable<NewsType> GetNewsTypes();

      [OperationContract]
      void EditNews(News news, bool makeDelivery);
   }
}
