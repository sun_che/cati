﻿using System;
using System.Collections.Generic;
using System.Linq;
using CATI.Data;

namespace CATI.Wcf
{
   /// <summary>
   /// Тестовый
   /// </summary>
   public class TestNewsRepository : INewsRepository
   {
      private IList<NewsType> _newsTypes;

      private IList<News> _news;

      public TestNewsRepository()
      {
         _newsTypes = new List<NewsType>()
         {
            new NewsType() {Id = 0, Name = "CATI"},
            new NewsType() {Id = 1, Name = "TNS"},
            new NewsType() {Id = 2, Name = "Портал"}
         };
         _news = new List<News>()
         {
            new News(){NewsDate = DateTime.Now, Head = "Заголовок новости1", Id = 0, Text = "Текст новости 1", Type = _newsTypes[0], TypeId = _newsTypes[0].Id},
            new News(){NewsDate = DateTime.Now, Head = "Заголовок новости2", Id = 1, Text = "Текст новости 2", Type = _newsTypes[0], TypeId = _newsTypes[0].Id},
            new News(){NewsDate = DateTime.Now, Head = "Заголовок новости3", Id = 2, Text = "Текст новости 3", Type = _newsTypes[0], TypeId = _newsTypes[0].Id},
            new News(){NewsDate = DateTime.Now, Head = "Заголовок новости4", Id = 3, Text = "Текст новости 4", Type = _newsTypes[1], TypeId = _newsTypes[1].Id},
            new News(){NewsDate = DateTime.Now, Head = "Заголовок новости5", Id = 4, Text = "Текст новости 5", Type = _newsTypes[2], TypeId = _newsTypes[2].Id}
         };
      }

      public IEnumerable<News> GetNews()
      {
         return _news;
      }

      public News GetNewsById(int newsId)
      {
         return _news.FirstOrDefault(x => x.Id == newsId);
      }

      public IEnumerable<NewsType> GetNewsTypes()
      {
         return _newsTypes;
      }

      public void EditNews(News news)
      {
      }
   }
}