﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using CATI.Data;

namespace CATI.Wcf
{
   public class EntityNewsRepository : INewsRepository
   {
      public IEnumerable<News> GetNews()
      {
         using (var context = new CatIContext())
         {
            var res = context.News.Include(x => x.Type).ToList();
            return res;
         }
      }

      public News GetNewsById(int newsId)
      {
         using (var context = new CatIContext())
         {
            var res = context.News.Include(x => x.Type).SingleOrDefault(x => x.Id == newsId);
            return res;
         }
      }

      public IEnumerable<NewsType> GetNewsTypes()
      {
         using (var context = new CatIContext())
         {
            return context.NewsTypes.ToList();
         }
      }

      public void EditNews(News news)
      {
         using (var context = new CatIContext())
         {
            //context.News.Attach(news);
            context.Entry(news).State = EntityState.Modified;
            context.SaveChanges();
         }
      }
   }
}
