﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CATI.Data;

namespace CATI.Wcf
{
   public class CatIContext : DbContext
   {
      public CatIContext()
         :base("DbConnection")
      {
      }

      public DbSet<News> News { get; set; }
      public DbSet<NewsType> NewsTypes { get; set; } 
   }
}
