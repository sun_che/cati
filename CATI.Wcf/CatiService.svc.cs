﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using CATI.Data;

namespace CATI.Wcf
{
   public class CatiService : ICatiService
   {
      private INewsRepository _repository = RepositoryFactory.GetNewsRepository();

      /// <summary>
      /// Передаем новости
      /// </summary>
      /// <returns></returns>
      public IEnumerable<News> GetNews()
      {
         return _repository.GetNews();
      }

      /// <summary>
      /// Конкрентная новость
      /// </summary>
      /// <param name="newsId">The news identifier.</param>
      /// <returns></returns>
      public News GetNewsById(int newsId)
      {
         return _repository.GetNewsById(newsId);
      }

      public IEnumerable<NewsType> GetNewsTypes()
      {
         return _repository.GetNewsTypes();
      }

      public void EditNews(News news, bool makeDelivery)
      {
         _repository.EditNews(news);
         if (makeDelivery)
         {
            MakeDelivery(news);
         }
      }

      public void MakeDelivery(News news)
      {
         
      }

   }
}
