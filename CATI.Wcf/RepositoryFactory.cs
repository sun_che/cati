﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CATI.Wcf
{
   /// <summary>
   /// Фабрика репозиториев
   /// </summary>
   public class RepositoryFactory
   {
      /// <summary>
      /// Gets the news repository.
      /// </summary>
      /// <returns></returns>
      public static INewsRepository GetNewsRepository()
      {
         //return new TestNewsRepository();
         return new EntityNewsRepository();
      }
   }
}
