﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using CATI.Data;

namespace CATI.Wcf
{
   public interface INewsRepository
   {
      IEnumerable<News> GetNews();

      News GetNewsById(int newsId);

      IEnumerable<NewsType> GetNewsTypes();

      void EditNews(News news);
   }
}
