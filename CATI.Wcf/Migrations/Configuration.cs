using CATI.Data;

namespace CATI.Wcf.Migrations
{
   using System;
   using System.Data.Entity;
   using System.Data.Entity.Migrations;
   using System.Linq;

   internal sealed class Configuration : DbMigrationsConfiguration<CATI.Wcf.CatIContext>
   {
      public Configuration()
      {
         AutomaticMigrationsEnabled = false;
      }

      protected override void Seed(CATI.Wcf.CatIContext context)
      {
         var typesList = new NewsType[]
                  {
                     new NewsType {Name = "CATI"},
                     new NewsType {Name = "TNS"},
                     new NewsType {Name = "������"}
                  };
         context.NewsTypes.AddOrUpdate(typesList);

         context.News.AddOrUpdate(
          new News() { NewsDate = DateTime.Now, Head = "��������� �������1", Text = "����� ������� 1", Type = typesList[0] },
          new News() { NewsDate = DateTime.Now, Head = "��������� �������2", Text = "����� ������� 2", Type = typesList[0] },
          new News() { NewsDate = DateTime.Now, Head = "��������� �������3", Text = "����� ������� 3", Type = typesList[0] },
          new News() { NewsDate = DateTime.Now, Head = "��������� �������4", Text = "����� ������� 4", Type = typesList[1] },
          new News() { NewsDate = DateTime.Now, Head = "��������� �������5", Text = "����� ������� 5", Type = typesList[2] });
      }
   }
}
