﻿using System.Web;
using System.Web.Mvc;

namespace CATI.Mvc
{
   public class FilterConfig
   {
      public static void RegisterGlobalFilters(GlobalFilterCollection filters)
      {
         filters.Add(new HandleErrorAttribute());
      }
   }
}
