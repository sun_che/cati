﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CATI.Data;
using CATI.Mvc.Models;

namespace CATI.Mvc.Controllers
{
   public class HomeController : Controller
   {
      ServiceReference.CatiServiceClient service = new ServiceReference.CatiServiceClient();
      public ActionResult Index()
      {
         var model = service.GetNews();
         ViewBag.selectedProject = new EditNews() {News = model.First()};
         return View(service.GetNews());
      }

      public ActionResult EditNewsDetails()
      {
         var Id = int.Parse(Request.Params["Id"]);
         var news = new EditNews() {News = service.GetNewsById(Id)};
         ViewBag.NewsTypes = service.GetNewsTypes();
         return PartialView("EditNewsDetails", news);
      }

      [HttpPost]
      public ActionResult EditNewsSaveChanges(EditNews editNews)
      {
         var newsTypeUd = int.Parse(Request.Params["cmbNewsType_VI"]);
         editNews.News.TypeId = newsTypeUd;
         service.EditNews(editNews.News, editNews.MakeDelivery);
         return RedirectToAction("Index");
      }
   }
}