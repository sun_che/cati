﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CATI.Data;

namespace CATI.Mvc.Models
{
   public class EditNews
   {
      public News News { get; set; }

      /// <summary>
      /// Gets or sets a value indicating whether [make delivery].
      /// </summary>
      /// <value>
      ///   <c>true</c> if [make delivery]; otherwise, <c>false</c>.
      /// </value>
      [Display(Name = "Выполнить рассылку")]
      public bool MakeDelivery { get; set; }
   }
}
