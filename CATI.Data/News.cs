﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CATI.Data
{
   /// <summary>
   /// Новость
   /// </summary>
   [DataContract]
   public class News
   {
      [DataMember]
      public int Id { get; set; }


      [DataMember]
      [Display(Name = "Тип")]
      public int TypeId { get; set; }
      
      [DataMember]
      [Display(Name = "Тип")]
      public NewsType Type { get; set; }

      [DataMember]
      [Display(Name = "Дата")]
      public DateTime NewsDate { get; set; }

      [DataMember]
      [Display(Name = "Заголовок")]
      public string Head { get; set; }

      [DataMember]
      [Display(Name = "Содержание")]
      public string Text { get; set; }

   }

   /// <summary>
   /// Тип новостей
   /// </summary>
   [DataContract]
   public class NewsType
   {
      [DataMember]
      [Display(Name = "Тип")]
      public int Id { get; set; }
      
      [DataMember]
      public string Name { get; set; }
   }
}
